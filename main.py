#!/usr/bin/env python3
from point import Point

def is_point_valid(maze, point):
    rows, columns = len(maze), len(maze[0])
    return 0 <= point.x and 0 <= point.y and rows > point.x and columns > point.y

def brute_force_travel(maze, visited, start_point, end_point):
    print("At point ({0}, {1})".format(start_point.x + 1, start_point.y + 1))
    if not is_point_valid(maze, start_point):
        print("Point is not valid")
        return False
    if visited[start_point.x][start_point.y]:
        print("Been here before")
        return False
    if start_point.is_equal(end_point):
        print("Hit final destination")
        visited[start_point.x][start_point.y] = True
        return True
    if maze[start_point.x][start_point.y] == 0:
        print("Blockade")
        return False
    if maze[start_point.x][start_point.y] == 1:
        print("Can use this tile")
        visited[start_point.x][start_point.y] = True
        # North
        if brute_force_travel(maze, visited, start_point.north(), end_point):
            return True
        # East
        if brute_force_travel(maze, visited, start_point.east(), end_point):
            return True
        # South
        if brute_force_travel(maze, visited, start_point.south(), end_point):
            return True
        # West
        if brute_force_travel(maze, visited, start_point.west(), end_point):
            return True
        print("Tile was no good")
        visited[start_point.x][start_point.y] = False
    return False

def breadth_first_travel(maze, visited, start_point, end_point):
    if not is_point_valid(maze, start_point):
        Return("Point is not valid")
        return False
    if start_point.is_equal(end_point):
        print("Reached destination")
        return True
    visited[start_point.x][start_point.y] = True
    queue = [start_point]
    while len(queue):
        current_point = queue[-1]
        print("At point ({0}, {1})".format(current_point.x + 1, current_point.y + 1))
        if current_point.is_equal(end_point):
            print("Reached destination")
            return True
        queue.pop()
        adj_points = [current_point.north(), current_point.east(), current_point.south(), current_point.west()]
        for adj_p in adj_points:
            if is_point_valid(maze, adj_p) and maze[adj_p.x][adj_p.y] == 1 and not visited[adj_p.x][adj_p.y]:
                visited[adj_p.x][adj_p.y] = True
                queue.append(adj_p)
    return False

def start():
    MAZE, visited, start_point, end_point = setup()
    if brute_force_travel(MAZE, visited, start_point, end_point):
        print("Solved using Brute Force!")
    MAZE, visited, start_point, end_point = setup()
    if breadth_first_travel(MAZE, visited, start_point, end_point):
        print("Solved using BFS!")
    return

def setup():
    maze = [[1, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1],
            [1, 0, 1, 0, 0, 0],
            [1, 0, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 1, 0]]
    rows, columns = len(maze), len(maze[0])
    visited = [[False for _ in range(columns)] for _ in range(rows)]
    start_point = Point(0, 0)
    end_point = Point(5, 4)
    return maze, visited, start_point, end_point

def main():
    start()
    return

if __name__ == "__main__":
    main()
