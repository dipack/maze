class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def is_equal(self, point):
        if self.x == point.x and self.y == point.y:
            return True
        return False

    def north(self):
        return Point(self.x - 1, self.y)

    def south(self):
        return Point(self.x + 1, self.y)

    def east(self):
        return Point(self.x, self.y + 1)

    def west(self):
        return Point(self.x, self.y - 1)
